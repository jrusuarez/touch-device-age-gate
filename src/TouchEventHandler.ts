export class TouchEventHandler {
  private callbacks: any = {};
  private touchSizes: number[] = [];

  constructor(private el: HTMLBodyElement) {
    this.listenToEvents();
  }

  public addEventListener(eventName: string, callback: any) {
    this.callbacks[eventName] = this.callbacks[eventName] || [];

    if (!this.callbacks[eventName].find((c: any) => c === callback)) {
      this.callbacks[eventName].push(callback);
    }
  }

  public removeEventListener(eventName: string, callback: any) {
    const index = this.callbacks[eventName].findIndex((c: any) => c === callback);
    if (index >= 0) {
      this.callbacks.splice(index, 1);
    }
  }

  public reset() {
    this.touchSizes = [];
  }

  public getAvgTouchSize(): number {
    if (this.touchSizes.length === 0) {
      return 0;
    }
    return this.touchSizes.reduce((acc, size) => acc + size, 0) / this.touchSizes.length;
  }

  public isAKid(): boolean {
    if (this.touchSizes.length === 0) {
      return false;
    }
    return this.getAvgTouchSize() < 1.5;
  }

  private touchListToArray(touchList: TouchList): Touch[] {
    const touches = [];
    for (let i = 0; i < touchList.length; i += 1) {
      touches.push(touchList[i]);
    }
    return touches;
  }

  private getTouchSize(touches: Touch[]): number {
    if (touches.length === 0) {
      return 0;
    }
    const accSize = touches.reduce(
      (acc: any, touch: Touch) => {
        return acc + Math.sqrt(touch.radiusX * touch.radiusX + touch.radiusY * touch.radiusY);
      },
      0,
    );
    return accSize / touches.length;
  }

  private getTouchForce(touches: Touch[]): number {
    if (touches.length === 0) {
      return 0;
    }
    const accSize = touches.reduce(
      (acc: any, touch: Touch) => {
        return acc + touch.force;
      },
      0,
    );
    return accSize / touches.length;
  }

  private handleStart(touchEvent: TouchEvent) {
    const touches = this.touchListToArray(touchEvent.changedTouches);
    const touchSize = this.getTouchSize(touches);
    const touchForce = this.getTouchForce(touches);
    this.throwEvent(
      'touchStart',
      {
        touchSize,
        touchForce,
        kid: touchSize < 1,
      },
    );
  }

  private handleMove(touchEvent: TouchEvent) {
    const touches = this.touchListToArray(touchEvent.changedTouches);
    const touchSize = this.getTouchSize(touches);
    this.touchSizes.push(touchSize);
  }

  private listenToEvents() {
    this.el.addEventListener('touchstart', this.handleStart.bind(this), false);
    this.el.addEventListener('touchmove', this.handleMove.bind(this), false);
  }

  private throwEvent(eventName: string, event: any) {
    this.callbacks[eventName].forEach((callback: any) => {
      callback(event);
    });
  }
}
