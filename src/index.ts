import { TouchEventHandler } from './TouchEventHandler';
const DD = require('dragdealer');

const bodyElement = document.getElementsByTagName('body')[0];
const touchEventHandler = new TouchEventHandler(bodyElement);

function updateTouchMove(touchSize: number) {
  const sizeElement = document.getElementById('touch-move');
  sizeElement.innerHTML = `${touchSize}`;
}

function updateResult(kid: boolean) {
  const resultElement = document.getElementById('result');
  resultElement.innerHTML = `I am ${kid ? 'a kid' : 'an adult'}`;
  resultElement.style.opacity = '1';
}

function reset() {
  document.getElementById('age-gate').style.display = 'block';
  document.getElementById('kid-warning').style.display = 'none';
  document.getElementById('adult-info').style.display = 'none';
  document.getElementById('reset-btn').style.display = 'none';
  touchEventHandler.reset();
  ageGateBtn.setValue(0);
}

function goToKidsZone() {
  document.getElementById('age-gate').style.display = 'none';
  document.getElementById('kid-warning').style.display = 'block';
  document.getElementById('adult-info').style.display = 'none';
  document.getElementById('reset-btn').style.display = 'block';
}

function goToAdultsZone() {
  document.getElementById('age-gate').style.display = 'none';
  document.getElementById('kid-warning').style.display = 'none';
  document.getElementById('adult-info').style.display = 'block';
  document.getElementById('reset-btn').style.display = 'block';
}

const resetBtnElement = document.getElementById('reset-btn');
resetBtnElement.addEventListener('click', reset, false);

const ageGateBtn = new DD('slide-to-unlock', {
  steps: 3,
  callback: (x: any, y: any) => {
    if (x === 0) {
      reset();
    }
    if (x === 1) {
      const touchSize = touchEventHandler.getAvgTouchSize();
      const isAKid = touchEventHandler.isAKid();
      updateTouchMove(touchSize);
      if (isAKid) {
        goToKidsZone();
      } else {
        goToAdultsZone();
      }
    }
  },
});
